package com.example.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseCondition {

	public static void main(String args[]) {
		String expression = "((E5 AND(E2 OR (E4 AND E6)))AND (E2 AND E4))";
		// expression = "(E1 AND E2)";
		List<String> recievedEvents = new ArrayList<>();
		recievedEvents.add("E1");
		recievedEvents.add("E2");
		recievedEvents.add("E3");
		recievedEvents.add("E4");

		// logic(expression, recievedEvents);
		System.out.println(matcherLogic(expression, recievedEvents));

	}

	public static boolean matcherLogic(String expression, List<String> recievedEvents) {
		expression = expression.replaceAll("\\s", "").replaceAll("AND", "&").replaceAll("OR", "|");
		System.out.println("Expression = " + expression);
		Pattern pattern = Pattern.compile("\\(([^\\(\\)]*)\\)");
		Matcher matcher = pattern.matcher(expression.substring(1, expression.length() - 1));
		StringBuffer buffer = new StringBuffer();

		long count = expression.chars().filter(ch -> ch == '(').count();
		System.out.println("count = " + count);

		while (matcher.find()) {
			String value = matcher.group();
			// System.out.println("value " + value);
			boolean step = matcherLogic(value, recievedEvents);
			System.out.println("step = " + step);
			matcher.appendReplacement(buffer, String.valueOf(step));
		}
		matcher.appendTail(buffer);
		expression = buffer.toString();
		System.out.println("buffer exp = " + expression);

		Stack<Boolean> stack = new Stack<>();
		System.out.println("exp final = " + expression);
		expression = expression.replaceAll("\\(", "").replaceAll("\\)", "");
		StringTokenizer token = new StringTokenizer(expression, "&|\\|", true);
		while (token.hasMoreElements()) {
			String event = token.nextToken();
			if (event.equals("&")) {
				String nextEvent = token.nextToken();
				Boolean x = nextEvent.matches("true|false") ? Boolean.valueOf(nextEvent)
						: recievedEvents.contains(nextEvent);
				boolean value2 = stack.pop();
				stack.push(x && value2);
			} else if (event.equals("|")) {
				String nextEvent = token.nextToken();
				Boolean x = nextEvent.matches("true|false") ? Boolean.valueOf(nextEvent)
						: recievedEvents.contains(nextEvent);
				boolean value2 = stack.pop();
				stack.push(x || value2);
			} else if (event.matches("true|false")) {
				stack.push(Boolean.valueOf(event));
			} else {
				stack.push(recievedEvents.contains(event));
			}

		}
		return stack.pop();

		// System.out.println("return = " + sb.toString());
	}

	public static void logic(String expression, List<String> recievedEvents) {
		expression = expression.replaceAll("\\s", "").replaceAll("AND", "&").replaceAll("OR", "|");
		System.out.println("Expression = " + expression);

		Stack<String> stack = new Stack();
		StringTokenizer token = new StringTokenizer(expression, "&|\\||(|)", true);
		while (token.hasMoreElements()) {
			String value = token.nextToken();
			System.out.println("Element : " + value);
			if (value.equals(")")) {
				String element = stack.pop();
				String exp = "";
				while (!element.equals("(")) {
					exp = exp + element;
					element = stack.pop();
				}
				exp = "(" + exp + ")";
				System.out.println("exp = " + exp);
				logic(exp, recievedEvents);

				// String value1 = stack.pop();
				// String operator = stack.pop();
				// String value2 = stack.pop();
				// if (operator.equals("AND")) {
				// Boolean x = recievedEvents.contains(value1) &&
				// recievedEvents.contains(value2);
				// stack.push(x.toString());
				// }
			} else {
				stack.push(value);
			}
		}

	}
}
