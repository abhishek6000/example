package com.example.service;

import org.junit.Assert;
import org.junit.Test;

public class ExampleServiceTest {

	private ExampleService service = new ExampleService();

	@Test
	public void test_value_valid() {
		int actual = service.value();
		Assert.assertEquals(1, actual);
	}

}
